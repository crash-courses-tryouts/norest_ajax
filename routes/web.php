<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', function () {
    return view('auth.login');
});


//Route::get('/home', 'NewsController@index')->name('home');
Route::get('/news/pagination','NewsController@pagination');
Route::get('/news/search/{search}','NewsController@search')->name('news.search');
Route::get('/news/deleted','NewsController@deleted')->name('news.deleted');
Route::get('/news/restore/{id}','NewsController@restore')->name('news.restore');
Route::get('/news/deleted/search/{search}','NewsController@search_del')->name('news.searchdel');
Route::resource('news', 'NewsController');