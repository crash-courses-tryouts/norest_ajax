<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at','created_at','updated_at'];
    protected $table="news";
    public $primaryKey="id";
    public $timestamps=true;

    public function user(){
        return $this->hasOne('App\User','id','id_user');
    }

    public function state(){
        return $this->hasOne('App\States','id','id_state');
    }

    //FUNCOES DE MODELO
    
    
    public function scopeGetAllNews($query,bool $trashed=null,bool $isajax=null):Object{
        //Necessário dar Join porque Ajax nao acede às collections criadas pelo laravel
        if($isajax){
            $query->select("news.id","title","name as publisher","description as state","news.created_at","news.deleted_at")
            ->join('users','id_user','users.id')
            ->join('states','id_state','states.id');
        }
        if($trashed){ 
             $query->onlyTrashed();
        }
            return $query->orderBy('created_at','desc')->paginate(5);
       
    }

    /**
     * Guardar Dados de Artigos
     * @param $data - Dados para armazenar do artigo
     * @param $id - id do artigo.Se vier nulo é um artigo novo.Outrora o artigo com esse id será atualizado
     */
    public function scopestoreArticle($query,array $data,int $id=null):bool{
        if($id){
            $query->where('news.id',$id);
            return  $query->update($data);
        }
        else{
            return $query->insert($data);
        }      
    }
    
    /**
     * Buscar artigo X
     * @param $id - Id do artigo a buscar.
     * Inclui trashed para os admins poderem visualizar o artigo na mesma.
     * Retirar esse parametro para os utilizadores normais
     */
    public function scopegetArticle($query,int $id):Object{
        return $query->withTrashed()->where('news.id',$id)->first();
        
    }

    /**
     * Destruir Artigo X
     * @param $id - Id do artigo a eliminar
     * 
     * Destroi o artigo.Como usamos a opcao de SoftDelete,o sistema do laravel
     * oculta todos os resultados das queries a nao ser que use-se a funcao withTrashed() ou onlyTrashed()
     * Restore retira o artigo dos eliminados
     */
    public function scopedestroyArticle($query,int $id):bool{
       return $query->where('news.id',$id)->delete();
    }


    /**
     * Restaura Artigo X
     * @param $id - Id do artigo a Restaurar
     * 
     * Restaura um artigo anteriormente apagado
     */
    public function scoperestoreArticle($query,int $id):bool{
        return $query->onlyTrashed()->where('news.id',$id)->restore();
    }

    /**
     * Pesquisa
     * @param $data - Array com  os parametros de pesquisa.
     *  Se é uma pesquisa AJAX terá um index adicional no array indicando.
     */
    public function scopesearchNews($query,array $data):Object{       
        $order="DESC";
        //Necessário dar Join porque Ajax nao acede às collections criadas pelo laravel
        if(isset($data['isajx']) && $data['isajx']===1){
         $query->select("news.id","title","name as publisher","description as state","news.created_at","news.deleted_at")
         ->join('users','id_user','users.id')
         ->join('states','id_state','states.id');
        }

        if($data['id_state']){
            $query=$query->where('id_state',$data['id_state']);
        }
        if($data['created_at']){
            $query=$query->whereDate('news.created_at', '=',$data['created_at']); # >= para incluir artigos a partir de X dia
            $order="ASC";
        }

        if($data['title']){
            $query=$query->where('title','like','%'.$data['title'].'%');
        }

        if($data['trashed']==1){
            $query=$query->onlyTrashed();
        }
        elseif($data['trashed']==2){
            $query=$query->withTrashed();
        }

        $query=$query->orderBy('created_at',$order);
        $query=$query->paginate(5);
        return $query;
        }
    }