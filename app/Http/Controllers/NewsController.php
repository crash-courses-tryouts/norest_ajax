<?php

namespace App\Http\Controllers;
use App\States;
use App\User;
use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Carrega as Noticias.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Se for um pedido Ajax retorna o Objeto
        if($request->ajax()){

            /*as funcoes usadas tiveram que ser modificadas,pois o ajax não reconhece a collection que o laravel
            gera quando existe uma relação.Por exemplo: fazer get de uma noticia, posso logo ir buscar o nome de quem criou.
            Pelo ajax é necessário dar join das tabelas. */

            return News::GetAllNews(null,true); //(isTrashed,IsAjax)
        }
        else{
            $news=News::GetAllNews();
            $states=States::All();
            $news->title="Artigos";
            return view('home',compact('news'),compact('states'));
        }
    }

    
    public function deleted(Request $request){
        if($request->ajax()){
            return News::GetAllNews(true,true);
        }
        else{
            $news=News::GetAllNews(1);
            $states=States::All();
            $news->title="Artigos Apagados";
            return view('home',compact('news'),compact('states'));
    }
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $states=States::All();
        return view('news.create',compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $this->validate($request,[
            'title'=>'required|min:3|max:50',
            'content'=>'required|max:200|min:20',
            'state'=>'required|integer',
        ]);

        $data=[
            "id"=>NULL,
            "title"=>$request->input('title'),
            "content"=>$request->input('content'),
            "id_user"=>auth()->user()->id,
            "id_state"=>$request->input('state'),
            "created_at" =>  \Carbon\Carbon::now(),
            "updated_at" => \Carbon\Carbon::now(),
        ];

        if(News::storeArticle($data)){
            if($request->ajax()){
                return json_encode([
                    'msg'=>'Noticia Criada Com Sucesso',
                    'status'=>'SUCCESS'
                ]);
            }else{
                return redirect('/news')->with('flash-success','Noticia Criada com Sucesso');
            }
        }else{
            if($request->ajax()){
                return json_encode([
                    'msg'=>'Erro ao Criar Com Sucesso',
                    'status'=>'ERROR'
                ]);
            }else{
                return redirect('/news')->with('flash-error','Erro ao criar Noticia');
            }
        }
            
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article=News::getArticle($id);
        return view('news.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $states=States::All();
        $article=News::getArticle($id);
        return view('news.edit',compact('article'),compact('states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $this->validate($request,[
                'title'=>'required|min:3|max:50',
                'content'=>'required|max:200|min:20',
                'state'=>'required|integer',
            ]);

            $data=[
                "title"=>$request->input('title'),
                "content"=>$request->input('content'),
                "id_state"=>$request->input('state'),
                "updated_at" => \Carbon\Carbon::now()
            ];

            if(News::storeArticle($data,$id)){
            if($request->ajax()){
                return json_encode([
                    'msg'=>'Noticia Atualizada Com Sucesso',
                    'status'=>'SUCCESS'
                ]);
            }else{
                return redirect('/news')->with('flash-success','Noticia Atualizada com Sucesso');
                }
            }else{
                if($request->ajax()){
                return json_encode([
                    'msg'=>'Erro ao Atualizar  Noticia',
                    'status'=>'ERROR'
                ]);
            }
            else{
                return redirect('/news')->with('flash-error','Erro ao Atualizar Noticia');  
            }
                
        }
    }


    function pagination(Request $request)
    {
        if($request->ajax()){
            $data = News::All()->paginate(5);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
     
        if(News::destroyArticle($id)){
            if($request->ajax()){
                return json_encode([
                    'msg'=>'Noticia Apagada Com Sucesso',
                    'status'=>'SUCCESS'
                ]);
            }
            else{
                return redirect('/news')->with('flash-success','Noticia Apagada com Sucesso');
            }
        }   
        else{
            if($request->ajax()){
                return json_encode([
                    'msg'=>'Ocorreu um erro ao tentar apagar a notícia',
                    'status'=>'ERROR'
                ]);
            }
            else{
                return redirect('/news')->with('flash-error','Erro ao tentar apagar Noticia');
            }
        }
           
    }

    public function restore(Request $request,$id){
            if(News::restoreArticle($id)){
                if($request->ajax()){
                    return json_encode([
                        'msg'=>'Noticia Restaurada Com Sucesso',
                        'status'=>'SUCCESS'
                    ]);
                }
                else{
                    return redirect('/news')->with('flash-success','Noticia Restaurada com Sucesso');
                }
            }
        else{
            if($request->ajax()){
                return json_encode([
                    'msg'=>'Ocorreu um erro ao tentar restaurar a notícia',
                    'status'=>'ERROR'
                ]);
            }
            else{
                return redirect('/news')->with('flash-error','Erro ao tentar restaurar Noticia');
            }
            
        }
    }

       
    public function search(Request $request)
    {
        $this->validate($request,['trashed'=>'required']);

        //array para seguir caminho para o Modelo,com associatovos referentes às colunas da tabela
        $data=[
            'title'=>$request->input('qtitle'),
            'created_at'=>$request->input('qdate'),
            'id_state'=>$request->input('qstate'),
            'trashed'=>$request->input('trashed')
        ];

        //array para preencher os dados de volta à view
        $search=[
            'qtitle'=>$request->input('qtitle'),
            'qdate'=>$request->input('qdate'),
            'qstate'=>$request->input('qstate'),
            'trashed'=>$request->input('trashed')
        ];

       if($request->ajax()){
            $data['isajx']=1; //parametro para indicar se e um pedido ajax   
            $news=News::searchNews($data);
            return $news;
        }
        else{
            $news=News::searchNews($data);
            $states=States::All();
            $news->title="Resultados Da Pesquisa";
            return view('home',compact('news'),compact('states'),compact('search'));
        }  
    }
}