<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    public function news(){
        return $this->hasMany('App\News',"id_state","id");
    }
}
