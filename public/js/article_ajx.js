
$(document).ready(function () {

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });


    $('#formcreate').submit(function (e) {
        e.preventDefault();
        let pure=true;
        window.message="";
        console.log('asdasdasd');
        pure=validator();


        if(pure){
            $.ajax({
                method: "POST",
                url: $('#formcreate').attr('action'),
                data: {
                    title: $('#title').val(),
                    content: $('#content').val(),
                    state: parseInt($('#state').val()),
                },
                success: (function (res) {
                    res=JSON.parse(res);
                    $('#msg').empty();
                    if(res.status=="SUCCESS"){
                        $('#msg').html('<div class="alert alert-success">'+res.msg+'</div>')
                        setTimeout(function () {window.location.replace('/news');}, 3000);
                    }
                    else{
                        $('#msg').html('<div class="alert alert-danger">'+res.msg+'</div>')
                    }
                    
                }),
                error: (function (data) {
                    console.log("Erro ao conectar-se ao Servidor");
                    console.log(data);
                }),
            })
        }
        else{
            console.log(message);
            
            $('#msg').html("<div class='alert alert-danger'>"+message+'</div>');
        }
    });





    $('#formedit').submit(function (e) {
        e.preventDefault();
        let pure=true;
        window.message="";
        pure=validator();

        if(pure){
            $.ajax({
                method: "PUT",
                url: $('#formedit').attr('action'),
                data: {
                    _method:"PUT",
                    title: $('#title').val(),
                    content: $('#content').val(),
                    state: parseInt($('#state').val()),
                },
                success: (function (res) {
                    res=JSON.parse(res);
                    $('#msg').empty();
                    if(res.status=="SUCCESS"){
                        $('#msg').html('<div class="alert alert-success">'+res.msg+'</div>')
                        setTimeout(function () {window.location.replace('/news');}, 3000);
                    }
                    else{
                        $('#msg').html('<div class="alert alert-danger">'+res.msg+'</div>')
                    }
                    
                }),
                error: (function (data) {
                    console.log("Erro ao conectar-se ao Servidor");
                    console.log(data);
                }),
            })
        }
        else{
            $('#msg').html("<div class='alert alert-danger'>"+message+'</div>');
        }
    });




    function validator(){
        if($('#title').val().includes('<script>')){
            window.message+="<p>Por favor retire quaisquer tags <script> </p>";
            return false
        }
        if($('#title').val().length<3){
            window.message+="<p>Por favor insira ao menos 3 caracteres Para o titulo</p>"
            return false
        }

        if($('#title').val().length>50){
            window.message+="<p>Ultrapassou o limite de 50 caracteres Para o titulo</p>"
            return false
        }

        
        if($('#content').val().includes('<script>')){
            window.message+="<p>Por favor retire quaisquer tags <script> </p>";
            return false
        }

        if($('#content').val().length<20){
            window.message+="<p>Por favor insira ao menos 20 caracteres Para o conteudo da noticia </p>"
            return false
        }

        if($('#content').val().length>200){
            window.message+="<p>Ultrapassou o limite de 200 caracteres Para o conteudo da noticia </p>"
            return false
        }

        if(!$.isNumeric($('#state').val())){
            window.message+="<p>Valor Inválido no Estado! Contacte um Administrador</p>";
            return false
        }
        if(parseInt($('#state'))<=0){
            window.message+="<p>Valor Inválido no Estado! Contacte um Administrador</p>";
            return false
        }
        return true;
    
    }

});