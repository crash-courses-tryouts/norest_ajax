$(document).ready(function () {


    /**
     * Funcao para adicionar o csrf no header dos pedidos ajax
     * Params: None
     * Returns: Void
     */
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });


    /**
     * Funcao Ajax que executará a pesquisa
     * Params:Evento do formulario a ser submetido
     * Returns: Void
     */
    $('#search').submit(function (e) {
        e.preventDefault();
       
        $.ajax({
            method: "GET",
            url: $('#search').attr('action'),
            data: {
                qtitle: $('#qtitle').val(),
                qdate: $('#qdate').val(),
                qstate: $('#qstate').val(),
                trashed: $('#trashed').val()
            },
            success: (function (res) {
                $('#title_page').val('Resultados da Sua Pesquisa');
                $('#action_buttons').empty();//Esvazia a div dos botoes acima da tabela,para pôr o botao de voltar a pag principal
                let button='<a href="/news" ><button class="btn btn-primary"><i class="fas fa-home"></i>Voltar à Home</button></a>';
                $('#action_buttons').html(button);


            let  link = this.url;
                $('#bodyNews').empty();//esvazia a tabela e caso tenha dados executa a funcao put Data e quando mais de por pagina,a paginate
                if (res.data.length) {
                    putData(res.data);
                    if(res.total>res.per_page){
                        paginate(link,res); 
                    }
                    else{
                        $('#pagination ul').detach();   //se tiver resultados mas nao o suficiente para paginacao,apagará quaisquer 'paginadores'       
                    }
                }
                else{
                    $('#bodyNews').html('<h3 class="text-center">Não foram encontradas Noticias relevantes a sua pesquisa</h3>');
                    $('#pagination ul').detach();
                }
                window.history.replaceState('','',link); 
                /*Põe o link da Pesquisa na barra do Browser.Assim,ao entrar num artigo,ou executar uma ação e voltar atrás estará na mesma pesquisa
                Embora tenha feito desta forma,(A.K.A [Guardar pesquisas no state do histórico para depois usar com o Pedido GET]),
                Este é um metódo a evitar,quando é possivel usar as sessions para guardar a ultima pesquisa efetuada e afins.
                Mais rápido,mais seguro e menos intrusivo. Infelizmente, apercebi-me tarde da situação.*/
            }),
            error: (function (data) {
                console.log("Erro ao conectar-se ao Servidor");
                console.log(data);
            }),
        })
    });
    

    /**
     * Funcao Ajax que irá buscar os dados consoante a paginação e pesquisa(ou nao) que estejamos a fazer
     * Params: Evento do botao de Paginacao
     * Returns: Void
     */
    $('#pagination').on('click','.page-item',function(e){
        e.preventDefault();
        let action="";
        if(e.currentTarget.nodeName==="LI")//se carregar no elemento LI
            action=e.currentTarget.firstElementChild.getAttribute('href');//vai buscar o href do elemento <a> presente dentro dele
        else if(e.currentTarget.nodeName==="A")
            action=e.getAttribute('href');//se "acertar" no <a>,busca o link direto
           
            //a action é null quando estamos por exemplo,na primeira pagina e carregamos no botao previous
        if(action!=null){
            $.ajax({
                    method: "GET",
                    url: action,
                    success: (function (res) {
                        let link = this.url;
                        $('#bodyNews').empty();
                        if (res.data.length) {
                        putData(res.data);
                        paginate(link,res);  
                        }
                        else{
                            $('#TableNews').parent().html('<h3 class="text-center">Não foram encontradas Noticias relevantes a sua pesquisa</h4>');
                            $('#pagination ul').detach();
                        }
                        window.history.replaceState('','',link);
                        //Mais uma vez,a mesma situação presente ao pesquisar acontece

                        })
                })
        }
    });


    /***
     * Funcao Ajax que irá apagar uma noticia
     * Params: Evento do formulario de apagar noticia a ser submetido
     * Returns: Void
     */
    $('body').on('submit','.delete_form',function(e){
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: e.currentTarget.getAttribute('action'),
            data: {
                _method: e.currentTarget[0].value
            },
            success: (function (res) {
                resDorR(JSON.parse(res));
    }),
    error:(function (xhr){
        console.log(xhr);
    })
});
    });


/***
     * Funcao Ajax que irá restaurar uma noticia
     * Params: Evento do botao de restaurar uma noticia ao ser clicado
     * Returns: Void
     */
    $('body').on('click','.restore_form',function(e){
        e.preventDefault();
        $.ajax({
            method: "GET",
            url: e.currentTarget.getAttribute('href'),
            success: (function (res) {
                   resDorR(JSON.parse(res));
            }),
            error:(function (xhr){
            console.log(xhr);
            })
        });

    });


/**
 * Funcao Auxiliar da de Pesquisa e Paginação,que irá pegar nos dados trazidos
 *  do pedido ajax e pô-los na tabela 
 * @param {Object} dados 
 * 
 */
    function putData(dados){
        for (let x = 0; x < dados.length; x++) {
            let line = "<tr><td><a href='/news/"+dados[x].id+"'>" + dados[x].title + "</a></td><td>" + dados[x].publisher + "</td><td>" + dados[x].created_at + "</td>";
            let token=$('meta[name="csrf-token"]').attr('content');
            if (dados[x].deleted_at == null) {
                line+="<td>" + dados[x].state + "</td><td>";
                line += "<td><a href='/news/" + dados[x].id + "/edit'><button class='btn btn-warning'><i class='fas fa-pencil-ruler'></i>&nbsp;Editar</button></a></td>";
                line += "<td><form method='POST' class='delete_form'  action='/news/" + dados[x].id + "'><input type='hidden' name='_method' value='DELETE'><input type='hidden' name='_token' value='"+token+"' ><button  id='dlt-btn'  type='submit' class='btn btn-danger'><i class='fas fa-trash-alt'></i>&nbsp;Apagar</button></form></td>"
            }
            else {
                line+="<td style='text-decoration: line-through'>" + dados[x].state + "</td>";
                line += "<td colspan='2' class='text-center'><a class='restore_form' href='/news/restore/" + dados[x].id + "'><button class='btn btn-primary'><i class='fas fa-recycle'></i>&nbsp;Restaurar</button></a></td>";
            }
            $('#bodyNews').append(line);
        }
    }

/**
 * Funcao que tratará de gerar a paginacao para os pedidos Ajax
 * @param {string} link 
 * @param {Object} res 
 */
    function paginate(link,res){

        if (res.total>res.per_page) {
            let urlcpesq ="";
            let matcher="";
               if(link.includes('?page=') || !(link.includes('/search/'))){
                //Verifica se estamos numa página que não seja de pesquisa
                urlcpesq=link.split('?page=')
                matcher='?page=';
               }
               else{
                //isto porque numa pesquisa GET,o parametro page fica para ultimo lugar
                urlcpesq = link.split("&page=");
                matcher='&page=';
               }
                
                if (urlcpesq.length == 2) {
                    link = urlcpesq[0];
                }
                let pag="";
                /*re-estruturamos os links vindos do servidor,
                pois a paginacao de laravel não atua corretamente ao fazermos pedidos Ajax,e portanto
                precisamos de re-construir os links vindos dos pedidos
                */

                res.first_page_url = link + matcher+"1";
                res.last_page_url = link + matcher + res.last_page;
                if (!(res.current_page >= res.last_page))
                {
                    res.next_page_url = link + matcher+ (res.current_page + 1);// last_page
                }
               $('#pagination ul').detach();//detach da paginacao anterior do DOM

                if (res.current_page == 1) {
                    pag += '<li class="page-item disabled" aria-disabled="true" aria-label="« Previous"><span class="page-link" aria-hidden="true">‹</span></li>';
              }
                else {
                    pag += '<li class="page-item"><a class="page-link" href="' +link+matcher+ (res.current_page-1) + '"  rel="prev" aria-label="« Previous">‹</a></li>';
                }
             
                for (var x = 1; x <= res.last_page; x++) {

                    if (x == res.current_page) {
                        pag += '<li class="page-item active"><a class="page-link"  href="' + link + matcher + x + '">' + x + '</a>';
                    }
                    else
                        pag += '<li class="page-item"><a class="page-link"   href="' + link + matcher + x + '">' + x + '</a>';
                }

                if (res.current_page == res.last_page) {
                    pag += '<li class="page-item disabled" aria-disabled="true" aria-label="Next »"><span  class="page-link" aria-hidden="true">›</span></li>';
                }
                else {
                    pag += '<li class="page-item"><a class="page-link"  href="' +link+matcher+ (res.current_page+1) + '" " rel="next" aria-label="Next »">›</a></li>';
                }

                $('#pagination').append('<ul class="pagination" id="paginated" role="navigation">');
                $('#pagination ul').append(pag);
        }



    }



    function resDorR(res){
        let div="<div class='container'>";
        if(res.status=="SUCCESS"){
            div+= '<div class="alert alert-success"><p class="lead"><i class="fas fa-check">&nbsp;'+res.msg+'</i></p></div>';
        }
        else if(res.status=="ERROR"){
            div+= '<div class="alert alert-warning"><p class="lead"><i class="fas fa-exclamation-triangle">&nbsp;'+res.msg+'</i></p></div>';
        }
        div+="</div>";
    $('#main-page').prepend(div);
        /*Verifica se tem algum link guardado no state do histórico.
        Ou seja,se eliminamos ou restauramos algo ao fazer uma pesquisa
        Se sim,retornará para a primeira pagina dessa mesma pesquisa 
        Se não,irá retornar para a página principal da seccao que estava usando 
        após ser notificado da mensagem*/

    let linkred =(history.state!=null)? history.state.split('&page='):window.location.href.split('&page=');
     setTimeout(function () {window.location.replace(linkred[0]);}, 3000);
    }

});