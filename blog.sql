-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 30-Nov-2018 às 18:40
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_29_151304_create_states_table', 1),
(4, '2018_11_29_151338_create_news_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_state` int(10) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `news`
--

INSERT INTO `news` (`id`, `title`, `content`, `id_user`, `id_state`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 'AAA', 'assadasdasdasd', 1, 1, NULL, '2018-11-30 08:52:39', '2018-11-30 12:04:32'),
(3, 'Ola', 'aoasdooasdasdsa', 1, 1, NULL, '2018-11-30 08:55:06', '2018-11-30 08:55:06'),
(4, 'Pesq', 'asasdopasdkopasd', 1, 2, NULL, '2018-11-29 09:17:32', '2018-11-30 09:17:32'),
(5, 'p arque ing', 'assdqweqweqew', 1, 1, NULL, '2018-11-30 09:17:47', '2018-11-30 09:17:47'),
(6, 'p wqeiropt iopasd', 'asdasddasdas', 1, 2, NULL, '2018-11-30 09:18:03', '2018-11-30 09:18:03'),
(7, 'awet', 'dsaaaaaaaaaaaaaaaaaaaaaaaadsaaaaaaaaaaaaaaaaaaaaaaaadsaaaaaaaaaaaaaaaaaaaaaaaa', 1, 1, NULL, '2018-11-29 09:18:28', '2018-11-30 09:18:28'),
(8, 'Ola', 'assdasdasdasd', 1, 2, NULL, '2018-11-30 09:18:42', '2018-11-30 09:18:42'),
(9, 'opqweoqwejioqwejiowejio', 'qweiopqwejioqejqwioejqwiojeioqwjeioqwjeio', 1, 1, NULL, '2018-11-30 09:18:55', '2018-11-30 09:18:55'),
(10, '1 eu 12 poaskdopaskdopadpasdpasdm,', 'dsadasdqweqweqweqweqwe', 1, 1, NULL, '2018-11-30 09:19:13', '2018-11-30 09:19:13'),
(11, 'asadsasdasdasdas', 'dasdasdasdasdasdasasdasd', 1, 2, NULL, '2018-11-29 09:19:30', '2018-11-30 09:19:30'),
(12, 'qweqweqwejeqwopekjop', 'keopqkeopqkeopqwkeopqwkeop', 1, 2, '2018-11-30 12:10:36', '2018-11-30 09:20:55', '2018-11-30 12:10:36'),
(13, 'Voltando ao tempo', 'aasssssssssssssssssssssssssssssssss', 1, 2, NULL, '2018-11-30 09:21:15', '2018-11-30 12:05:10'),
(14, 'Tudo bem', 'Aqui ta bem', 2, 1, NULL, '2018-11-30 12:18:52', '2018-11-30 12:18:52'),
(15, 'Nova Noticia', 'oiiii', 2, 1, '2018-11-30 14:28:27', '2018-11-30 14:28:11', '2018-11-30 14:28:27'),
(16, 'aaaaaaaaaaaa', 'dsasdasdsad', 2, 1, NULL, '2018-11-30 14:35:51', '2018-11-30 14:35:51'),
(17, 'weqweqweew', 'qweqweqeqweqw', 2, 1, NULL, '2018-11-30 14:37:46', '2018-11-30 14:37:46'),
(18, 'qweeqqwe', 'qeqweqweqweqweqweqw', 2, 2, NULL, '2018-11-30 14:40:05', '2018-11-30 14:40:05'),
(19, 'qweeqqwe', 'qeqweqweqweqweqweqw', 2, 2, NULL, '2018-11-30 14:43:29', '2018-11-30 14:43:29'),
(20, 'aaaaa', 'adasasddasas', 2, 1, NULL, '2018-11-30 14:43:41', '2018-11-30 14:43:41'),
(21, 'A rua da Juana', 'qweqweeqweqweqw', 2, 1, NULL, '2018-11-30 14:44:30', '2018-11-30 14:44:30'),
(22, 'asdasdasd', 'dasdasdasdasdasdasd', 2, 1, '2018-11-30 14:50:12', '2018-11-30 14:48:57', '2018-11-30 14:50:12'),
(23, 'asasd', 'eqweqeqw', 2, 1, NULL, '2018-11-30 14:50:25', '2018-11-30 14:50:25'),
(24, 'asdasdasdasd', 'dasdasdasdasdqw', 2, 2, NULL, '2018-11-30 14:58:10', '2018-11-30 14:58:10'),
(25, 'asadsasdasdasdas', 'dasdasdasasdasddasasd', 2, 1, NULL, '2018-11-30 14:58:57', '2018-11-30 14:58:57'),
(26, 'aaaa', 'asdasasdsd', 2, 1, NULL, '2018-11-30 15:00:07', '2018-11-30 15:00:07'),
(27, 'asdasdasdasd', 'adasdasddasdas', 2, 1, NULL, '2018-11-30 15:05:43', '2018-11-30 15:05:43'),
(28, 'asdasdasdasd', 'dasdasdasasd', 2, 1, '2018-11-30 15:12:16', '2018-11-30 15:06:32', '2018-11-30 15:12:16'),
(29, 'aaa', 'asdasdasdsdd', 2, 1, '2018-11-30 15:11:42', '2018-11-30 15:10:34', '2018-11-30 15:11:42'),
(30, 'aaaaa', 'dasdasdasdasssssssssssssssssssssssdd', 2, 2, '2018-11-30 16:00:34', '2018-11-30 15:13:13', '2018-11-30 16:00:34'),
(31, 'A rua da Juana', 'aaaaa', 1, 1, NULL, '2018-11-30 15:53:09', '2018-11-30 15:53:09'),
(32, 'FASFASDASD', 'AAAAA', 1, 1, NULL, '2018-11-30 15:53:27', '2018-11-30 15:59:46');

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `states`
--

CREATE TABLE `states` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `states`
--

INSERT INTO `states` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Publicado', '2018-11-30 00:00:00', '2018-11-30 00:00:00'),
(2, 'Nao Publicado', '2018-11-30 00:00:00', '2018-11-30 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Maro', 'mario@mario.com', NULL, '$2y$10$xAqbgsdgl3ChnJQJIA19cuc5WkIQ9Ru.6SG/pHondrWGZ0USyuZjO', '53P9CUOUZx4SBL5BUyU2peBrzHZ3sGFclenrhy3KOh3kRZlgUswEm0nxVY80', '2018-11-30 08:49:49', '2018-11-30 08:49:49'),
(2, 'Afonso', 'af@onso.com', NULL, '$2y$10$3nQyTaEce30qKYyqsovZgOM2v/aB3H.17J6BctUPD.WMRAkwT7Oyy', 'BY9bHcRYfyHp6EKW8KQZJnvWb2PKv51fUdCB652hSpaq7gQYifulK7gCDZak', '2018-11-30 12:18:31', '2018-11-30 12:18:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_id_user_foreign` (`id_user`),
  ADD KEY `news_id_state_foreign` (`id_state`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `states`
--
ALTER TABLE `states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_id_state_foreign` FOREIGN KEY (`id_state`) REFERENCES `states` (`id`),
  ADD CONSTRAINT `news_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
