@extends('layouts.app')
@section('content')
    
<div class="container">
        <script src="{{asset('js/article_ajx.js')}}"></script>
    <h2>Criar Artigo</h2>
    <a onclick=" window.history.back();"><button class="btn btn-primary"><i class="fas fa-chevron-left"></i>&nbsp; Voltar Atrás</button></a>
    <div class="mt-3" id="msg"></div>
    <form method="POST" id="formcreate" action="{{route('news.store')}}">
    @csrf
    <label for="title">Titulo:</label>
    <input type="text" class="form-control" required id="title" name="title"><br>
    <label for="content">Conteudo: </label>
    <textarea class="form-control" required id="content" name="content"></textarea>
    <label>Estado:</label>
    <select class="form-control" required id="state" name="state">
    @if(count($states))
        @foreach($states as $state)
            <option value="{{$state->id}}">{{$state->description}}</option>
         @endforeach
    @else
        <option value="1">Publicado</option>
        <option value="2">Não Publicado</option>
    @endif
    </select><br>
    <button class="btn btn-primary" type="submit">Adicionar Noticia</button>
</form>
</div>
@endsection