@extends('layouts.app')
@section('content')
    
<div class="container">
        <script src="{{asset('js/article_ajx.js')}}"></script>
        <h2>Editar Artigo</h2>
        <a onclick=" window.history.back();"><button class="btn btn-primary"><i class="fas fa-chevron-left"></i>&nbsp; Voltar Atrás</button></a>
        <div class="mt-3" id="msg"></div>
        @if(isset($article))
  
<form method="POST" id="formedit" action="{{route('news.update',$article->id)}}">
        {{method_field('PUT')}}
    @csrf
    <label for="title">Titulo:</label>
<input type="text" class="form-control" minlength="3" maxlength="50" value="{{$article->title}}" id="title" name="title"><br>
    <label for="content">Conteudo: </label>
<textarea class="form-control" id="content" name="content" minlength="20" maxlength="200">{{$article->content}}</textarea>
    <label>Estado:</label>
    <select class="form-control" id="state" name="state">
    @if(count($states))
        @foreach($states as $state)
            <option value="{{$state->id}}"@if($article->id_state ==$state->id) selected AQUI @endif>{{$state->description}}</option>
        @endforeach
    @else
        <option value="1">Publicado</option>
        <option value="2">Não Publicado</option>
    @endif  
    </select><br>
    <button class="btn btn-primary" type="submit">Guardar Alterações</button>
</form>
@else
<h1>Artigo Não encontrado!</h1>
@endif

</div>
@endsection